-- country insert
insert into country(name)
values ('Ukraine'),
 ('Poland'),
 ('Great Britain'),
 ('Sweden'),
 ('Germany'),
 ('Spain'),
 ('Italy'),
 ('Belarus'),
 ('France'),
 ('Norvegia');
-- city insert
insert into city(name, country_id)
values ('Kyiv', 1),
 ('Warsaw', 2),
 ('London', 3),
 ('Stockholm', 4),
 ('Berlin', 5),
 ('Madrid', 6),
 ('Rome', 7),
 ('Minsk', 8),
 ('Paris', 9),
 ('Oslo', 10);
-- steet insert
insert into street(name, city_id)
values ("Khreshchatyk", 1),
 ("Nowy Świat", 2),
 ("Oxford", 3),
 ("Drottninggatan", 4),
 ("Greifenhagener", 5),
 ("Calle de Embajadores", 6),
 ("Via Daniele Manin", 7),
 ("Independence Avenue", 8),
 ("Champs Elysees", 9),
 ("Karl Johans", 10);
-- adress insert
insert into address(building_number, flat_number, street_id)
values (1, '1', 1),
 (1, '1', 2),
 (1, '1', 3),
 (1, '1', 4),
 (1, '1', 5),
 (1, '1', 6),
 (1, '1', 7),
 (1, '1', 8),
 (1, '1', 9),
 (1, '1', 10);

-- user insert
insert into user(first_name, last_name, address_id, date_of_birth)
values ('Roman', 'Yasura', 1, '1999-06-11'),
 ('Roman', 'Petrov', 2, '1998-06-11'),
 ('Andrii', 'Krenta', 3, '1997-06-11'),
 ('Bohdan', 'Petryshyn', 4, '1996-09-19'),
 ('Yura', 'Kuziv', 5, '1989-08-11'),
 ('Pavlo', 'Ivanenko', 6, '1979-07-11'),
 ('Ivan', 'Yandersky', 7, '1969-06-11'),
 ('Oleg', 'Koreta', 8, '1987-06-11'),
 ('Viktoria', 'Sushko', 9, '1975-06-11'),
 ('Anna', 'Skoryk', 10, '1991-09-13');
-- credential insert
insert into credential(user_id, login, pass)
values (1, 'romy', sha1('romy11061999')),
 (2, 'romp', sha1('romp11061998')),
 (3, 'andk', sha1('andk11061997')),
 (4, 'bohp', sha1('bohp19091996')),
 (5, 'yurk', sha1('yurk11081989')),
 (6, 'pavi', sha1('pavi11071979')),
 (7, 'ivay', sha1('iavy11061969')),
 (8, 'olek', sha1('olek11061987')),
 (9, 'viks', sha1('viks11061975')),
 (10, 'anns', sha1('annk18091991'));
-- email insert
insert into email(email, user_id)
values ('rom@gmail.com', 1),
 ('romp@gmail.com', 2),
 ('andk@gmail.com', 3),
 ('bohp@gmail.com', 4),
 ('yurk@gmail.com', 5),
 ('pavi@gmail.com', 6),
 ('ivay@gmail.com', 7),
 ('olek@gmail.com', 8),
 ('viks@gmail.com', 9),
 ('anns@gmail.com', 10);
-- document_type insert
insert into document_type(type)
values ('ID card'),
 ('Driver license'),
 ('Passport');
-- document insert
insert into document(file_path, document_type_id, user_id)
values
('/documents/photo/1.png', 1, 1),
('/documents/photo/2.png', 1, 2),
('/documents/photo/3.png', 1, 3),
('/documents/photo/4.png', 1, 4),
('/documents/photo/5.png', 1, 5),
('/documents/photo/6.png', 1, 6),
('/documents/photo/7.png', 1, 7),
('/documents/photo/8.png', 1, 8),
('/documents/photo/9.png', 1, 9),
('/documents/photo/10.png', 1, 10);
-- phone_number insert
insert into phone_number(number, user_id)
values ('+380985635478', 1),
('+380985123458', 2),
('+380985637841', 3),
('+380985517526', 4),
('+380975654478', 5),
('+380985635689', 6),
('+380985645438', 7),
('+380985679851', 8),
('+380985664875', 9),
('+380985644931', 10);
-- apartment_type insert
insert into apartment_type(type)
values ('1 room'),
('Whole apartment'),
('2 room'),
('3 room');
-- apartment insert
insert into apartment(
  amount_of_guest,
  amount_bedroom,
  amount_of_bed,
  amount_of_bathroom,
  apartment_type_id,
  address_id,
  user_id,
  price_per_day,
  description)
 values
 (1, 1, 1, 1, 1, 1, 1, 20.0, 'Nice room in Kiev'),
 (1, 1, 1, 1, 1, 2, 2, 30.0, 'Nice room in Warsaw'),
 (1, 1, 1, 1, 1, 3, 3, 40.0, 'Nice room in Stockholm'),
 (1, 1, 1, 1, 1, 4, 4, 50.0, 'Nice room in Berlin'),
 (1, 1, 1, 1, 1, 5, 5, 60.0, 'Nice room in Madrid'),
 (1, 1, 1, 1, 1, 6, 6, 70.0, 'Nice room in Rome'),
 (1, 1, 1, 1, 1, 7, 7, 15.0, 'Nice room in Minsk'),
 (1, 1, 1, 1, 1, 8, 8, 50.0, 'Nice room in Paris'),
 (1, 1, 1, 1, 1, 10, 10, 70.0, 'Nice room in Oslo');
-- feedback insert
insert into feedback(text, apartment_id, user_id)
values
("A realy nice room, highly recommend it", 1, 10),
("A realy nice room, highly recommend it", 2, 10),
("A realy nice room, highly recommend it", 3, 10),
("A realy nice room, highly recommend it", 4, 10),
("A realy nice room, highly recommend it", 5, 10),
("A realy nice room, highly recommend it", 6, 10);
-- rating_type insert
insert into rating_type(type)
values
('cleaning'),
('location'),
('owner');
-- rating insert
insert into rating(user_id, apartment_id, value, rating_type_id)
values
(10, 1, 5, 1),
(10, 1, 4, 2),
(10, 1, 5, 3),
(10, 2, 5, 1),
(10, 2, 5, 3),
(10, 3, 5, 1),
(10, 4, 5, 1),
(10, 5, 5, 1);
-- apartment_photo insert
insert into apartment_photo(path, apartment_id)
values
('/apartments/1.png', 1),
('/apartments/2.png', 1),
('/apartments/3.png', 1),
('/apartments/4.png', 1),
('/apartments/5.png', 1),
('/apartments/6.png', 1),
('/apartments/7.png', 2),
('/apartments/8.png', 2),
('/apartments/9.png', 2),
('/apartments/10.png', 2),
('/apartments/11.png', 2),
('/apartments/12.png', 2),
('/apartments/13.png', 2);
-- convenience insert
insert into convenience(type)
values
('wi-fi'),
('fire alarm'),
('iron'),
('kitchen'),
('TV'),
('Heating'),
('Boiler'),
('microwave oven');
-- apartment_convenience insert
insert into apartment_convenience(apartment_id, convenience_id, presence)
values
(1,1,1),
(1,2,1),
(1,3,1),
(1,4,1),
(2,1,1),
(2,2,1),
(2,3,1),
(3,1,1),
(4,1,1),
(3,3,1),
(4,2,1),
(3,4,1);
-- reservation_status insert
insert into reservation_status(status)
values
('reserved'),
('canceled'),
('approved');
-- reservation insert
insert into reservation(
  start_date,
  end_date,
  amount_of_guest,
  apartment_id,
  user_id,
  reservation_status_id
)
values
 ('2019-10-26', '2019-10-27', 1, 1, 10, 1),
 ('2019-10-28', '2019-10-29', 1, 2, 10, 1),
 ('2019-11-01', '2019-11-02', 1, 3, 10, 1),
 ('2019-11-10', '2019-11-13', 1, 4, 10, 1);

-- payment_status insert
insert into payment_status (status)
  values
 ('on service balance'),
 ('on apartment owner balance'),
 ('canceled');

-- payment insert
insert into payment(from_user_id, to_user_id, payment_status_id, apartment_id, sum)
  values
 (10, 1, 1, 1, 20),
 (10, 1, 1, 2, 50),
 (10, 1, 1, 3, 70),
 (10, 1, 1, 4, 100);
