CREATE SCHEMA IF NOT EXISTS `apartment_reservation` DEFAULT CHARACTER SET utf8;
USE `apartment_reservation` ;
DROP TABLE IF EXISTS `apartment_reservation`.`payment` ;
DROP TABLE IF EXISTS `apartment_reservation`.`payment_status` ;
DROP TABLE IF EXISTS `apartment_reservation`.`reservation` ;
DROP TABLE IF EXISTS `apartment_reservation`.`reservation_status` ;
DROP TABLE IF EXISTS `apartment_reservation`.`apartment_convenience` ;
DROP TABLE IF EXISTS `apartment_reservation`.`convenience` ;
DROP TABLE IF EXISTS `apartment_reservation`.`apartment_photo` ;
DROP TABLE IF EXISTS `apartment_reservation`.`rating` ;
DROP TABLE IF EXISTS `apartment_reservation`.`rating_type` ;
DROP TABLE IF EXISTS `apartment_reservation`.`feedback` ;
DROP TABLE IF EXISTS `apartment_reservation`.`apartment` ;
DROP TABLE IF EXISTS `apartment_reservation`.`apartment_type` ;
DROP TABLE IF EXISTS `apartment_reservation`.`phone_number` ;
DROP TABLE IF EXISTS `apartment_reservation`.`document` ;
DROP TABLE IF EXISTS `apartment_reservation`.`document_type` ;
DROP TABLE IF EXISTS `apartment_reservation`.`email` ;
DROP TABLE IF EXISTS `apartment_reservation`.`credential` ;
DROP TABLE IF EXISTS `apartment_reservation`.`user` ;
DROP TABLE IF EXISTS `apartment_reservation`.`address` ;
DROP TABLE IF EXISTS `apartment_reservation`.`street` ;
DROP TABLE IF EXISTS `apartment_reservation`.`city` ;
DROP TABLE IF EXISTS `apartment_reservation`.`country` ;

CREATE SCHEMA IF NOT EXISTS `apartment_reservation` DEFAULT CHARACTER SET utf8;
USE `apartment_reservation` ;

-- -----------------------------------------------------
-- Table `apartment_reservation`.`country`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `apartment_reservation`.`country` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `apartment_reservation`.`country`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `apartment_reservation`.`country` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `apartment_reservation`.`city`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `apartment_reservation`.`city` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  `country_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_city_country_idx` (`country_id` ASC) VISIBLE,
  CONSTRAINT `fk_city_country`
    FOREIGN KEY (`country_id`)
    REFERENCES `apartment_reservation`.`country` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `apartment_reservation`.`street`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `apartment_reservation`.`street` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  `city_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_street_city1_idx` (`city_id` ASC) VISIBLE,
  CONSTRAINT `fk_street_city1`
    FOREIGN KEY (`city_id`)
    REFERENCES `apartment_reservation`.`city` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `apartment_reservation`.`address`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `apartment_reservation`.`address` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `building_number` INT NOT NULL,
  `flat_number` VARCHAR(7) NULL,
  `street_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_address_street1_idx` (`street_id` ASC) VISIBLE,
  CONSTRAINT `fk_address_street1`
    FOREIGN KEY (`street_id`)
    REFERENCES `apartment_reservation`.`street` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `apartment_reservation`.`user`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `apartment_reservation`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `photo_path` VARCHAR(255) NULL,
  `address_id` INT NOT NULL,
  `money` DECIMAL(9,2) NOT NULL DEFAULT 0.0,
  `date_of_birth` DATE NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_user_data_address1_idx` (`address_id` ASC) VISIBLE,
  CONSTRAINT `fk_user_data_address1`
    FOREIGN KEY (`address_id`)
    REFERENCES `apartment_reservation`.`address` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `apartment_reservation`.`credential`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `apartment_reservation`.`credential` (
  `user_id` INT NOT NULL,
  `login` VARCHAR(20) NOT NULL,
  `pass` VARCHAR(100) NOT NULL,
  INDEX `fk_credential_user_data1_idx` (`user_id` ASC) VISIBLE,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_credential_user_data1`
    FOREIGN KEY (`user_id`)
    REFERENCES `apartment_reservation`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `unique_login`
    UNIQUE(login))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `apartment_reservation`.`email`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `apartment_reservation`.`email` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(60) NOT NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_email_user1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_email_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `apartment_reservation`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `apartment_reservation`.`document_type`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `apartment_reservation`.`document_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `apartment_reservation`.`document`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `apartment_reservation`.`document` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `file_path` VARCHAR(255) NOT NULL,
  `document_type_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_document_document_type1_idx` (`document_type_id` ASC) VISIBLE,
  INDEX `fk_document_user1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_document_document_type1`
    FOREIGN KEY (`document_type_id`)
    REFERENCES `apartment_reservation`.`document_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_document_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `apartment_reservation`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `apartment_reservation`.`phone_number`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `apartment_reservation`.`phone_number` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `number` VARCHAR(45) NOT NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_phone_number_user1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_phone_number_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `apartment_reservation`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `apartment_reservation`.`apartment_type`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `apartment_reservation`.`apartment_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `apartment_reservation`.`apartment`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `apartment_reservation`.`apartment` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `amount_of_guest` INT NOT NULL,
  `amount_bedroom` INT NOT NULL,
  `amount_of_bed` INT NOT NULL,
  `amount_of_bathroom` INT NOT NULL,
  `apartment_type_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  `price_per_day` DECIMAL(7,2) NOT NULL,
  `description` VARCHAR(400) NULL,
   `address_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_apartment_apartment_type1_idx` (`apartment_type_id` ASC) VISIBLE,
  INDEX `fk_apartment_user1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_apartment_apartment_type1`
    FOREIGN KEY (`apartment_type_id`)
    REFERENCES `apartment_reservation`.`apartment_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_apartment_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `apartment_reservation`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_apartment_address1`
    FOREIGN KEY (`address_id`)
    REFERENCES `apartment_reservation`.`address` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `apartment_reservation`.`feedback`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `apartment_reservation`.`feedback` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `text` VARCHAR(400) NOT NULL,
  `apartment_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_feedback_apartment1_idx` (`apartment_id` ASC) VISIBLE,
  INDEX `fk_feedback_user1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_feedback_apartment1`
    FOREIGN KEY (`apartment_id`)
    REFERENCES `apartment_reservation`.`apartment` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_feedback_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `apartment_reservation`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `apartment_reservation`.`rating_type`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `apartment_reservation`.`rating_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `apartment_reservation`.`rating`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `apartment_reservation`.`rating` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `apartment_id` INT NOT NULL,
  `value` INT(1) NOT NULL,
  `rating_type_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_rating_user1_idx` (`user_id` ASC) VISIBLE,
  INDEX `fk_rating_apartment1_idx` (`apartment_id` ASC) VISIBLE,
  INDEX `fk_rating_rating_type1_idx` (`rating_type_id` ASC) VISIBLE,
  CONSTRAINT `fk_rating_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `apartment_reservation`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rating_apartment1`
    FOREIGN KEY (`apartment_id`)
    REFERENCES `apartment_reservation`.`apartment` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rating_rating_type1`
    FOREIGN KEY (`rating_type_id`)
    REFERENCES `apartment_reservation`.`rating_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `apartment_reservation`.`apartment_photo`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `apartment_reservation`.`apartment_photo` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `path` VARCHAR(255) NOT NULL,
  `apartment_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_apartment_photo_apartment1_idx` (`apartment_id` ASC) VISIBLE,
  CONSTRAINT `fk_apartment_photo_apartment1`
    FOREIGN KEY (`apartment_id`)
    REFERENCES `apartment_reservation`.`apartment` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `apartment_reservation`.`convenience`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `apartment_reservation`.`convenience` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `apartment_reservation`.`apartment_convenience`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `apartment_reservation`.`apartment_convenience` (
  `apartment_id` INT NOT NULL,
  `convenience_id` INT NOT NULL,
  `presence` TINYINT NOT NULL,
  PRIMARY KEY (`apartment_id`, `convenience_id`),
  INDEX `fk_convenience_has_apartment_apartment1_idx` (`apartment_id` ASC) VISIBLE,
  INDEX `fk_convenience_has_apartment_convenience1_idx` (`convenience_id` ASC) VISIBLE,
  CONSTRAINT `fk_convenience_has_apartment_convenience1`
    FOREIGN KEY (`convenience_id`)
    REFERENCES `apartment_reservation`.`convenience` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_convenience_has_apartment_apartment1`
    FOREIGN KEY (`apartment_id`)
    REFERENCES `apartment_reservation`.`apartment` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `apartment_reservation`.`reservation_status`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `apartment_reservation`.`reservation_status` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `status` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `apartment_reservation`.`reservation`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `apartment_reservation`.`reservation` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `start_date` DATE NOT NULL,
  `end_date` DATE NOT NULL,
  `amount_of_guest` INT NOT NULL,
  `apartment_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  `reservation_status_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_reservation_apartment1_idx` (`apartment_id` ASC) VISIBLE,
  INDEX `fk_reservation_user1_idx` (`user_id` ASC) VISIBLE,
  INDEX `fk_reservation_reservation_status1_idx` (`reservation_status_id` ASC) VISIBLE,
  CONSTRAINT `fk_reservation_apartment1`
    FOREIGN KEY (`apartment_id`)
    REFERENCES `apartment_reservation`.`apartment` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reservation_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `apartment_reservation`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reservation_reservation_status1`
    FOREIGN KEY (`reservation_status_id`)
    REFERENCES `apartment_reservation`.`reservation_status` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `apartment_reservation`.`payment_status`
-- -----------------------------------------------------


CREATE TABLE IF NOT EXISTS `apartment_reservation`.`payment_status` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `status` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `apartment_reservation`.`payment`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `apartment_reservation`.`payment` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `from_user_id` INT NOT NULL,
  `to_user_id` INT NOT NULL,
  `payment_status_id` INT NOT NULL,
  `apartment_id` INT NOT NULL,
  `sum` DECIMAL(7,2) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_payment_user1_idx` (`from_user_id` ASC) VISIBLE,
  INDEX `fk_payment_user2_idx` (`to_user_id` ASC) VISIBLE,
  INDEX `fk_payment_payment_status1_idx` (`payment_status_id` ASC) VISIBLE,
  CONSTRAINT `fk_payment_user1`
    FOREIGN KEY (`from_user_id`)
    REFERENCES `apartment_reservation`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_payment_user2`
    FOREIGN KEY (`to_user_id`)
    REFERENCES `apartment_reservation`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_payment_payment_status1`
    FOREIGN KEY (`payment_status_id`)
    REFERENCES `apartment_reservation`.`payment_status` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_payment_apartment1`
    FOREIGN KEY (`apartment_id`)
    REFERENCES `apartment_reservation`.`apartment` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
